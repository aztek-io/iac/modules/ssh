########################################
### Buckets ############################
########################################

resource "aws_s3_bucket" "keys" {
    count  = var.create_bucket ? 1 : 0
    bucket = local.bucket_name
}

########################################
### Objects ############################
########################################

# module SSH keys
####################

resource "aws_s3_bucket_object" "private_key" {
    depends_on  = [
        local_file.private_key
    ]

    bucket      = var.create_bucket ? aws_s3_bucket.keys[0].id : local.bucket_name
    key         = basename(local.pem)
    source      = local.pem
}

resource "aws_s3_bucket_object" "public_key" {
    depends_on  = [
        local_file.public_key
    ]

    bucket      = var.create_bucket ? aws_s3_bucket.keys[0].id : local.bucket_name
    key         = basename(local.pub)
    source      = local.pub
}

