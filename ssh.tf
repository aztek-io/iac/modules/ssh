# module SSH keys
####################

resource "tls_private_key" "module" {
    algorithm   = "RSA"
    rsa_bits    = 4096
}

resource "local_file" "public_key" {
    sensitive_content   = tls_private_key.module.public_key_openssh
    filename            = local.pub
}

resource "local_file" "private_key" {
    sensitive_content   = tls_private_key.module.private_key_pem
    filename            = local.pem
}

resource "aws_key_pair" "module" {
    key_name   = local.key_name
    public_key = tls_private_key.module.public_key_openssh
}
