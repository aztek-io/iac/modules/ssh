variable "region" {
    default = "us-west-2"
}

variable "project" {}
variable "environment" {}
variable "key_suffix" {}

variable "create_bucket" {
    type    = bool
    default = false
}

locals {
    bucket_name = join("-", [var.project, var.environment, "ssh-keys"])
    key_name    = join("-", [var.project, var.environment, var.key_suffix])
    pub         = join("-", [var.project, var.environment, "${var.key_suffix}.pub"])
    pem         = join("-", [var.project, var.environment, "${var.key_suffix}.pem"])
}
