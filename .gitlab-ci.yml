image:
  name: hashicorp/terraform:light
  entrypoint:
    - '/usr/bin/env'

variables:
  TERRAFORM_DIR: "./"
  PROJECT: "aztek-iac-modules"
  ENVIRONMENT: "ssh-key"

before_script:
  - set -e
  - |
        if command -v terraform; then
            terraform --version
            terraform init "$TERRAFORM_DIR"
        elif command -v aws; then
            aws --version
        fi

stages:
  - test_syntax
  - copy_keys
  - terraform_plan
  - terraform_apply
  - terraform_destroy
  - version

TestTerraform:
  stage: test_syntax
  script:
    - tflint "$TERRAFORM_DIR"
  image:
    name: wata727/tflint
    entrypoint:
      - '/usr/bin/env'
  only:
    changes:
      - .gitlab-ci.yml
      - "*.tf"
  except:
  - tags

CopyKeys:
  stage: copy_keys
  script:
    - aws s3 sync "s3://${PROJECT}-${ENVIRONMENT}-ssh-keys" . || true
  artifacts:
    expire_in: 1 hour
    paths:
    - ./*.pub
  image:
    name: aztek/awscli
    entrypoint:
      - '/usr/bin/env'
  only:
    changes:
      - .gitlab-ci.yml
      - "*.tf"
  except:
  - tags

TerraformValidate:
  stage: test_syntax
  script:
    - terraform validate "$TERRAFORM_DIR"
  only:
    changes:
      - .gitlab-ci.yml
      - "*.tf"
  except:
  - tags

TerraformPlan:
  stage: terraform_plan
  script:
    - |
        terraform plan \
            -var "project=$PROJECT" \
            -var "environment=$ENVIRONMENT-$(date +%s)" \
            -var "create_bucket=true" \
            -var "key_suffix=ci-build" \
            -out="${PROJECT}-${ENVIRONMENT}.tf_plan" \
            "$TERRAFORM_DIR"
  artifacts:
    name: "${PROJECT}-${ENVIRONMENT}"
    paths:
      - "*.tf_plan"
  only:
    changes:
      - .gitlab-ci.yml
      - "*.tf"
  except:
  - tags

TerraformApply:
  stage: terraform_apply
  script:
    - terraform apply --auto-approve "${PROJECT}-${ENVIRONMENT}.tf_plan"
  dependencies:
    - TerraformPlan
  artifacts:
    name: "${PROJECT}-${ENVIRONMENT}-terraform.tfstate"
    paths:
      - "terraform.tfstate*"
    expire_in: 1 week
  only:
    changes:
      - .gitlab-ci.yml
      - "*.tf"
  except:
  - tags

TerraformDestroy:
  stage: terraform_destroy
  script:
    - |
        terraform destroy \
            -var "project=$PROJECT" \
            -var "environment=$ENVIRONMENT" \
            -var "create_bucket=true" \
            -var "key_suffix=ci-build" \
            --auto-approve \
            "$TERRAFORM_DIR"
  dependencies:
    - TerraformApply
  only:
    changes:
      - .gitlab-ci.yml
      - "*.tf"
  except:
  - tags

########################################
### Human Readable Version #############
########################################

version:
  stage: version
  script:
    - version
    - finalize
  image:
    name: aztek/version-utils
    entrypoint:
    - /usr/bin/env
  artifacts:
    paths:
      - tag_version.txt
      - tag_version.json
    expire_in: 1 week
  except:
  - tags
